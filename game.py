from random import randint

name = input("What is your name?")


for guess_number in range(1, 6):
    month_guess = randint(1,12)
    year_guess = randint(1924, 2004)


    print("Guess 1 :" ,name, "Were you born in", month_guess, "/", year_guess)
    response = input("yes or no?")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number ==5:
        print("I have other things to do. Good bye.")

    else:
        print("Let me try again!")
